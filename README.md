# emergent sc extensions

My personal extensions for SuperCollider.

## Usage

### ControlPoster
Prints the arguments of a given SynthDef to the post window so that you can copy-paste them into your code.

* ControlPoster.synth: takes a SynthDef as input, prints out a Synth.new with the SynthDef's arguments as args.
* ControlPoster.pbind: takes a SynthDef as input, prints out a Pbind with the SynthDef's arguments as parameters.
* ControlPoster.pbindef: takes a SynthDef as input, prints out a Pbindef with the SynthDef's arguments as parameters.

