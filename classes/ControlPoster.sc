ControlPoster {

	* synth {
		arg synthname = \default;
		var exclude = ['\out', '\gate', '\fx', '\?'];
		var params = SynthDescLib.global.synthDescs.at(synthname).controls.reject({
			arg control;
			var name = control.name;
			exclude.includes(name)
		});

		"\(".postln; // opening bracket
		"~%Synth = Synth\(%%, \[".format(synthname, "\\", synthname).postln;

		params.do({
			arg control;
			var name = control.name;
			var val = control.defaultValue;

			val = case
			{ name == \freq }{ "(Scale.minorPentatonic.degrees.choose + [39, 51, 63, 75].choose).midicps" }
			{ name != \freq } { val.round(0.01) };

			"\t%%, %,".format("\\", name, val).postln;
		});

		"\]\);".postln;
		"\)\n".postln // closing bracket + newline + closing bracket
	}

    * pbind {
        arg synthname = \default;
				var exclude = ['\out', '\gate', '\fx', '\?'];
		var params = SynthDescLib.global.synthDescs.at(synthname).controls.reject({
			arg control;
			var name = control.name;
			exclude.includes(name)
		});

        "\(".postln; // opening bracket
        "~%Pat = Pbind\(".format(synthname).postln; // declaring the pbind
        "\t%%, %%,".format("\\", "instrument", "\\", synthname).postln;
        "\t%%, 1,".format("\\", "dur").postln;

        params.do({
            arg control;
            var name = control.name;
            var val = control.defaultValue;

            val = case
                { name == \freq } { "Pxrand((Scale.minorPentatonic.degrees+60).midicps, inf)" }
                { name != \freq } { val.round(0.01) };
            "\t%%, %,".format("\\", name, val).postln;
      });
      "\);\n\)".postln // closing bracket + newline + closing bracket
    }

    * pbindef {
        arg synthname = \default;
		var exclude = ['\out', '\gate', '\fx', '\?'];
		var params = SynthDescLib.global.synthDescs.at(synthname).controls.reject({
			arg control;
			var name = control.name;
			exclude.includes(name)
		});

        "\(".postln; // opening bracket
        "Pbindef(%%,".format("\\", synthname).postln; // declaring the pbindef
        "\t%%, %%,".format("\\", "instrument", "\\", synthname).postln;
        "\t%%, 1,".format("\\", "dur").postln;

        params.do({
            arg control;
            var name = control.name;
            var val = control.defaultValue;

            val = case
                { name == \freq } { "Pxrand((Scale.minorPentatonic.degrees+60).midicps, inf)" }
                { name != \freq } { val.round(0.01) };
            "\t%%, %,".format("\\", name, val).postln;
      });
      "\);\n\)".postln // closing bracket + newline + closing bracket
    }
}
